package org.test.utils.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Organism;
import chado.loader.model.Stock;
import chado.loader.model.Stockprop;
import chado.loader.service.CvTermService;
import chado.loader.service.OrganismService;
import chado.loader.service.StockPropService;
import chado.loader.service.StockService;


public class TestInsert_stockProp {

	@Test
	public void testVariant_variant_set() {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
		properties.put("javax.persistence.jdbc.url", "jdbc:postgresql://192.168.15.34:5432/iric");
		properties.put("javax.persistence.jdbc.user", "iricadmin");
		properties.put("javax.persistence.jdbc.password", "iricadmin");

		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://192.168.15.34:5432/iric", "iricadmin", "iricadmin");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<String> popValue = new ArrayList<>();
		popValue.add("aro");
		popValue.add("aus");
		popValue.add("indx");
		popValue.add("subtrop");
		popValue.add("ind2");
		popValue.add("trop");
		popValue.add("ind3");
		popValue.add("japx");
		popValue.add("ind1B");
		popValue.add("ind1A");
		popValue.add("temp");
		popValue.add("admix");
		

		AppContext.createEntityManager(properties);
		
		StockService stock_ds = new StockService();
		
		CvTermService cvterm_ds = new CvTermService();
		
		Cvterm cvtermPop = cvterm_ds.findCvtermyId(67120).get(0);
		
		OrganismService o_ds = new OrganismService();
		
		Organism o = o_ds.findOrganismId(23).get(0);
		
		
		List<Stock> stock_lst = stock_ds.findByStockyByOrganism(o);
		
		StockPropService stockprop_ds = new StockPropService();
		
		Stockprop stockProp = null;
		
		int i =1;
		
		for (Stock stock : stock_lst) {
			Random rand = new Random();
			
			stockProp = new Stockprop();
			stockProp.setStock(stock);
			stockProp.setRank(0);
			stockProp.setCvterm(cvtermPop);
			stockProp.setValue(popValue.get(rand.nextInt(popValue.size())));
			
			stockprop_ds.insertRecord(stockProp);
			
			System.out.println(i++ +":" + stockProp.getValue());
			
			stockProp = null;
			
			
			
			rand = null;
			
			
		}

//		VariantSetService v_ds = new VariantSetService();
//
//		SnpFeatureService ds = new SnpFeatureService();
//
//		VariantVariantSetService vvs_ds = new VariantVariantSetService();
//
//		Variantset variantSet = v_ds.getVariantSetById(19).get(0);
//
//		System.out.println("NAME " + variantSet.getName());
//
//		LoaderVariantVariantSet vvs;
//		Integer id = vvs_ds.getVariantVariantSetCurrentSeqNumber().intValue() + 1;
//
//		List<LoaderVariantVariantSet> v_list = new ArrayList<>();
//		for (SnpFeature sf : ds.getSnpFeatureByVariantSetId(variantSet)) {
//			vvs = new LoaderVariantVariantSet();
//			vvs.setVariantVariantsetId(id++);
//			vvs.setVariantFeatureId(sf.getSnpFeatureId());
//			vvs.setVariantset(variantSet.getVariantsetId());
//			vvs.setHdf5Index(sf.getSnpFeatureId() - 1);
//
//			System.out.println("INSERT: " + vvs.getVariantFeatureId());
//			System.out.println(vvs.getVariantset());
//			System.out.println(vvs.getHdf5Index());
//
//			v_list.add(vvs);
//			vvs = null;
//
//		}
//
//		PgBulkInsert<LoaderVariantVariantSet> bulkInsertVariantVariantSet = new PgBulkInsert<LoaderVariantVariantSet>(
//				new VariantVariantSetMapping("public", "variant_variantset"));
//
//		try {
//			bulkInsertVariantVariantSet.saveAll(PostgreSqlUtils.getPGConnection(conn), v_list.stream());
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		System.out.println("END>>");

	}

}
