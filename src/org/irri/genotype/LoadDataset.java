package org.irri.genotype;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.swt.SWTException;
import org.irri.genotype.loader.object.LoaderSnpFeature;
import org.irri.genotype.loader.object.LoaderSnpFeatureLoc;
import org.irri.genotype.loader.object.LoaderVariantVariantSet;
import org.irri.genotype.mapping.SnpFeatureLocMapping;
import org.irri.genotype.mapping.SnpfeatureMapping;
import org.irri.genotype.mapping.VariantVariantSetMapping;

import chado.loader.model.Cvterm;
import chado.loader.model.Db;
import chado.loader.model.Dbxref;
import chado.loader.model.Feature;
import chado.loader.model.GenotypeRun;
import chado.loader.model.Organism;
import chado.loader.model.Platform;
import chado.loader.model.SampleVarietyset;
import chado.loader.model.Stock;
import chado.loader.model.StockSample;
import chado.loader.model.Variantset;
import chado.loader.service.DbXrefService;
import chado.loader.service.FeatureService;
import chado.loader.service.GenotypeRunService;
import chado.loader.service.PlatformService;
import chado.loader.service.SampleVarietySetService;
import chado.loader.service.SnpFeatureService;
import chado.loader.service.StockSampleService;
import chado.loader.service.StockService;
import chado.loader.service.VariantVariantSetService;
import de.bytefish.pgbulkinsert.PgBulkInsert;
import de.bytefish.pgbulkinsert.util.PostgreSqlUtils;

public class LoadDataset {

	private File samplefile;
	private File positionfile;
	private File h5File;
	private Cvterm cvterm;
	private Organism organism;
	private Db db;
	private Variantset vset;
	private Cvterm ctTermfeature;
	private Cvterm nonSynTerm;
	private Cvterm spliceDnrTerm;
	private Cvterm spliceAccTerm;
	private Connection conn;
	private long startTime;

	private Logger logger = Logger.getLogger(LoadDataset.class.getName());
	private LoaderProperties prop;
	private int lines;

	public LoadDataset(String sampleFile, String posFile, String h5, Cvterm cvterm, Organism organism, Db db,
			Variantset vSet, Cvterm ctTermfeature, Cvterm nonSynTerm, Cvterm spliceAccTerm, Cvterm spliceDnrTerm,
			LoaderProperties prop) {

		samplefile = new File(sampleFile);
		positionfile = new File(posFile);
		h5File = new File(h5);

		this.cvterm = cvterm;
		this.organism = organism;
		this.db = db;
		this.vset = vSet;
		this.ctTermfeature = ctTermfeature;
		this.nonSynTerm = nonSynTerm;
		this.spliceDnrTerm = spliceDnrTerm;
		this.spliceAccTerm = spliceAccTerm;

		this.prop = prop;

	}

	public void open() {
		final StockService ds = new StockService();
		final FeatureService featureDs = new FeatureService();
		final SnpFeatureService sf_ds = new SnpFeatureService();
		final DbXrefService dbXrefDs = new DbXrefService();
		final StockSampleService stockSampleDs = new StockSampleService();
		final SampleVarietySetService sampleVaritySetDs = new SampleVarietySetService();

		VariantVariantSetService vvs_ds = new VariantVariantSetService();

		Integer id = sf_ds.getSnpFeatureCurrentSeqNumber().intValue();

		Integer vvs_id = vvs_ds.getVariantVariantSetCurrentSeqNumber().intValue() + 1;

		conn = null;

		startTime = System.nanoTime();

		try {
			logger.info("jdbc:postgresql://" + prop.getHostname() + "/" + prop.getDatabasename() + ","
					+ prop.getUsername() + "," + prop.getPassword());
			conn = DriverManager.getConnection("jdbc:postgresql://" + prop.getHostname() + "/" + prop.getDatabasename(),
					prop.getUsername(), prop.getPassword());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
//			File samplefile = fcl.getFile();
//			File positionfile = fcl2.getFile();
//			File nonSynfile = fcl3.getFile();
//			File spliceAcceptorfile = fcl4.getFile();
//			File spliceDonorFile = fcl5.getFile();

			BufferedReader sampleFileReader = getBufferedReader(samplefile);
			BufferedReader posFileReader = getBufferedReader(positionfile);
//			final BufferedReader nonSynfileReader = getBufferedReader(nonSynfile);
//			final BufferedReader spliceAccReader = getBufferedReader(spliceAcceptorfile);
//			final BufferedReader spliceDnrReader = getBufferedReader(spliceDonorFile);

			int sampleFileNumber = getlineNumber(samplefile);
			int positionFile = getlineNumber(positionfile);
			lines = sampleFileNumber + getlineNumber(positionfile);

//			startProgressBar(lines);

			String line;

			int counter = 1;

			// List<LoaderStock> stockList = new ArrayList<>();

			Integer hdfCounter = 0;
			if (sampleFileReader != null) {
				logger.info("Loading Sample File.... \\n");

				logger.info("Loading " + sampleFileNumber + " records .... \n");
				while ((line = sampleFileReader.readLine()) != null) {

					final String textContent = line;

					Stock stock = AddStock(ds, textContent);
					Dbxref dbXref = AddDbXref(dbXrefDs, textContent);

					StockSample stockSample = AddStockSample(stockSampleDs, stock, dbXref, hdfCounter);
					SampleVarietyset svs = AddSampleVarietySet(sampleVaritySetDs, stockSample, db, hdfCounter);

					line = null;
					hdfCounter++;

					counter++;

				}
			}

			// updateProgressBar();

			LoaderSnpFeature loaderSnpFeature;
			LoaderSnpFeatureLoc loaderSnpFeatureLoc;
			LoaderVariantVariantSet loadervvs;

			// // Create the BulkInserter:
			PgBulkInsert<LoaderSnpFeature> bulkInsert = new PgBulkInsert<LoaderSnpFeature>(
					new SnpfeatureMapping("public", "snp_feature"));

			// PgBulkInsert<LoaderStock> bulkInsertStock = new PgBulkInsert<LoaderStock>(
			// new StockMapping("public", "stock"));

			PgBulkInsert<LoaderSnpFeatureLoc> bulkInsertSnpFeatureLoc = new PgBulkInsert<LoaderSnpFeatureLoc>(
					new SnpFeatureLocMapping("public", "snp_featureloc"));

			PgBulkInsert<LoaderVariantVariantSet> bulkInsertVariantVariantSet = new PgBulkInsert<LoaderVariantVariantSet>(
					new VariantVariantSetMapping("public", "variant_variantset"));
			List<LoaderSnpFeature> snpFeatureList = null;
			List<LoaderSnpFeatureLoc> snpfeatureLocList = null;
			List<LoaderVariantVariantSet> vvs_list = null;
			// HashMap<String, HashMap<String, Integer>> snpFeatureMap = new HashMap<>();
//			HashMap<String, Integer> snpFeatureMap = new HashMap<>();
			// HashMap<String, Integer> posMap;

			Feature feature = null;

			if (posFileReader != null) {
				// int posCounter = 1;
				// int lines = getlineNumber(positionfile);

				logger.info("Loading SnpFeature.... \n");
				logger.info("Loading " + positionFile + " records .... \n");
				// for (int i = 1; i <= lines; i++) {
				int i = id + 1;

				String pos = "";
				int idx = 0;
				while ((line = posFileReader.readLine()) != null) {
					String token[] = line.split("\t");

					if (snpFeatureList == null)
						snpFeatureList = new ArrayList<>();
					if (snpfeatureLocList == null)
						snpfeatureLocList = new ArrayList<>();
					if (vvs_list == null)
						vvs_list = new ArrayList<>();

					if (!pos.equals(token[0])) {

						List<Feature> result = featureDs.getFeatureByNameTypeOrganism("Chr" + token[0], ctTermfeature,
								organism);

						if (result.size() == 0) {
							pos = token[0];
							feature = new Feature();
							feature.setCvterm(ctTermfeature);
							feature.setOrganism(organism);
							feature.setName("Chr" + token[0]);
							feature.setUniquename("Chr0" + token[0]);

							featureDs.insertRecord(feature);
						} else
							feature = result.get(0);

					}

					// if (!snpFeatureMap.containsKey(pos)) {
					// posMap = new HashMap<>();
					// posMap.put(token[1].trim(), i);
					//
					// } else {
					// posMap = snpFeatureMap.get(pos);
					// posMap.put(token[1].trim(), i);
					//
					// }
					// snpFeatureMap.put(token[1].trim(), i);

					loaderSnpFeature = new LoaderSnpFeature();
					loaderSnpFeature.setSnpFeatureId(i);
					loaderSnpFeature.setVariantSetId(vset.getVariantsetId());

					snpFeatureList.add(loaderSnpFeature);

					loadervvs = new LoaderVariantVariantSet();
					loadervvs.setVariantVariantsetId(vvs_id);
					loadervvs.setVariantFeatureId(i);
					loadervvs.setVariantset(vset.getVariantsetId());
					loadervvs.setHdf5Index(idx);

					vvs_list.add(loadervvs);

					loaderSnpFeatureLoc = new LoaderSnpFeatureLoc();
					loaderSnpFeatureLoc.setOrganismId(organism.getOrganismId());
					loaderSnpFeatureLoc.setSrcFeatureid(feature.getFeatureId());
					loaderSnpFeatureLoc.setRefCall(token[2]);
					loaderSnpFeatureLoc.setPosition(Integer.parseInt(token[1]) - 1);
					loaderSnpFeatureLoc.setSnpfeatureId(i);

					snpfeatureLocList.add(loaderSnpFeatureLoc);

					loaderSnpFeature = null;
					loaderSnpFeatureLoc = null;
					loadervvs = null;
					feature = null;

					if (idx % 5000000 == 0 && idx != 0) {
						try {
							logger.info("inserting 5000000 snpfeature");

							bulkInsert.saveAll(PostgreSqlUtils.getPGConnection(conn), snpFeatureList.stream());

							// bulkInsertStock.saveAll(PostgreSqlUtils.getPGConnection(conn),
							// stockList.stream());

							logger.info("inserting 5000000 snpfeature_loc");
							bulkInsertSnpFeatureLoc.saveAll(PostgreSqlUtils.getPGConnection(conn),
									snpfeatureLocList.stream());

							logger.info("inserting 5000000 vvs");
							bulkInsertVariantVariantSet.saveAll(PostgreSqlUtils.getPGConnection(conn),
									vvs_list.stream());

							logger.info("deleting 5000000 snpfeature");
							snpFeatureList = null;
							logger.info("deleting 5000000 snpfeatureloc");
							snpfeatureLocList = null;
							logger.info("deleting 5000000 vvs");
							vvs_list = null;
							logger.info("deleting done");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (idx % 100000 == 0) {
						if (idx % 1000000 == 0) {
							if (idx % 5000000 == 0) {
								logger.info(idx + "will insert bulk");
							}
						} else
							logger.info(idx + ": snp_featureList=" + snpFeatureList.size() + "; SNP_FeatureLOC: "
									+ snpfeatureLocList.size() + "; VVS: " + vvs_list.size());
					}

					vvs_id++;
					counter++;
					i++;
					idx++;

				}
				
				
				try {
					logger.info("Inserting Final Records");

					logger.info("inserting "+snpFeatureList.size()+" snpfeature_List");
					
					bulkInsert.saveAll(PostgreSqlUtils.getPGConnection(conn), snpFeatureList.stream());

					// bulkInsertStock.saveAll(PostgreSqlUtils.getPGConnection(conn),
					// stockList.stream());

					logger.info("inserting "+snpfeatureLocList.size()+" snpfeature_loc");
					bulkInsertSnpFeatureLoc.saveAll(PostgreSqlUtils.getPGConnection(conn),
							snpfeatureLocList.stream());

					logger.info("inserting "+snpFeatureList.size()+" vvs");
					bulkInsertVariantVariantSet.saveAll(PostgreSqlUtils.getPGConnection(conn),
							vvs_list.stream());

					logger.info("deleting  snpfeature");
					snpFeatureList = null;
					logger.info("deleting snpfeatureloc");
					snpfeatureLocList = null;
					logger.info("deleting  vvs");
					vvs_list = null;
					logger.info("deleting done");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

//			try {

//				// // Create the BulkInserter:
//				PgBulkInsert<LoaderSnpFeature> bulkInsert = new PgBulkInsert<LoaderSnpFeature>(
//						new SnpfeatureMapping("public", "snp_feature"));
//
//				// PgBulkInsert<LoaderStock> bulkInsertStock = new PgBulkInsert<LoaderStock>(
//				// new StockMapping("public", "stock"));
//
//				PgBulkInsert<LoaderSnpFeatureLoc> bulkInsertSnpFeatureLoc = new PgBulkInsert<LoaderSnpFeatureLoc>(
//						new SnpFeatureLocMapping("public", "snp_featureloc"));
//
//				PgBulkInsert<LoaderVariantVariantSet> bulkInsertVariantVariantSet = new PgBulkInsert<LoaderVariantVariantSet>(
//						new VariantVariantSetMapping("public", "variant_variantset"));

			/**
			 * 
			 */

//				bulkInsert.saveAll(PostgreSqlUtils.getPGConnection(conn), snpFeatureList.stream());
//
//				// bulkInsertStock.saveAll(PostgreSqlUtils.getPGConnection(conn),
//				// stockList.stream());
//
//				bulkInsertSnpFeatureLoc.saveAll(PostgreSqlUtils.getPGConnection(conn), snpfeatureLocList.stream());
//
//				bulkInsertVariantVariantSet.saveAll(PostgreSqlUtils.getPGConnection(conn), vvs_list.stream());

			// // Now save all entities of a given stream:
			// bulkInsert.saveAll(PostgreSqlUtils.getPGConnection(connection),
			// persons.stream());

//			} catch (SQLException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}

//			writeProgress("Loading Non Synch.... \n");
//			if (nonSynfileReader != null) {
//				insertSnpFeatureProp(nonSynfileReader, snpFeatureMap, nonSynTerm);
//			}

//			updateProgressBar();

//			writeProgress("Loading Splice Acc.... \n");
//			if (spliceAccReader != null) {
//				insertSnpFeatureProp(spliceAccReader, snpFeatureMap, spliceAccTerm);
//			}

//			writeProgress("Loading Splice Donor.... \n");

//			if (spliceDnrReader != null) {
//				insertSnpFeatureProp(spliceDnrReader, snpFeatureMap, spliceDnrTerm);
			// display.asyncExec(new Runnable() {
			//
			// public void run() {
			// String line;
			// LoaderSnpFeatureProp snpFeatureProp;
			// List<LoaderSnpFeatureProp> listfeatureProp = new ArrayList<>();
			// try {
			// while ((line = spliceDnrReader.readLine()) != null) {
			// String token[] = line.split(",");
			//
			// HashMap<String, Integer> pos = snpFeatureMap.get(token[0]);
			// // System.out.println("SIze "+ token[0]+"->"+ pos.size());
			// snpFeatureProp = new LoaderSnpFeatureProp();
			// snpFeatureProp.setTypeId(spliceDnrTerm.getCvtermId());
			// snpFeatureProp.setSnpFeatureId(pos.get(token[1].trim()));
			// snpFeatureProp.setValue(token[2]);
			//
			// listfeatureProp.add(snpFeatureProp);
			//
			// // System.out.println(snpFeatureProp.getTypeId() +": "+
			// // snpFeatureProp.getSnpFeatureId() +" : "+ snpFeatureProp.getValue());
			//
			// snpFeatureProp = null;
			// counter++;
			//
			// }
			// spliceDnrReader.close();
			//
			// try {
			//
			// PgBulkInsert<LoaderSnpFeatureProp> bulkInsertSnpFeatureProp = new
			// PgBulkInsert<LoaderSnpFeatureProp>(
			// new SnpFeaturePropMapping("public", "snp_featureprop"));
			//
			// bulkInsertSnpFeatureProp.saveAll(PostgreSqlUtils.getPGConnection(conn),
			// listfeatureProp.stream());
			//
			// // // Now save all entities of a given stream:
			// // bulkInsert.saveAll(PostgreSqlUtils.getPGConnection(connection),
			// // persons.stream());
			//
			// } catch (SQLException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			//
			// } catch (NumberFormatException e) {
			//
			// e.printStackTrace();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			//
			// }
			// });
//			}

			sampleFileReader.close();
			posFileReader.close();
			// nonSynfileReader.close();
			// spliceAccReader.close();
			// spliceDnrReader.close();

			Platform platform = insertPlatForm();
			if (h5File != null)
				insertGenotypeRun(platform);

//			insertSnpGenotype();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {

			e1.printStackTrace();
		} catch (SWTException ew) {
			ew.printStackTrace();
		}

		// performComplete();

	}

	private void insertGenotypeRun(Platform platformm) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		GenotypeRunService ds = new GenotypeRunService();

		GenotypeRun entity = new GenotypeRun();
		entity.setPlatformId(platformm.getPlatformId());
		entity.setDatePerformed(date);
		entity.setDataLocation(h5File.getName().toString());

		ds.insertRecord(entity);

	}

	private Platform insertPlatForm() {
		PlatformService ds = new PlatformService();

		Platform platform = new Platform();
		platform.setDb(db);
		platform.setVariantset(vset);

		ds.insertRecord(platform);

		return platform;

	}

	private BufferedReader getBufferedReader(File file) {

		if (file != null)
			try {
				return new BufferedReader(new FileReader(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		return null;
	}

	private int getlineNumber(File file) {
		int numberOflines = 0;
		if (file != null) {
			try {
				LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file));
				lineNumberReader.skip(Long.MAX_VALUE);

				numberOflines = lineNumberReader.getLineNumber();

				lineNumberReader.close();

				return numberOflines;

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return numberOflines;
	}

	private Stock AddStock(StockService ds, String textContent) {

		Stock stock = new Stock();
		stock.setCvterm(cvterm);
		stock.setOrganism(organism);
		stock.setIsObsolete(false);
		stock.setName(textContent);
		stock.setUniquename(textContent);

		List<Stock> result = ds.findByStockyByOrganismTypeName(cvterm, organism, textContent);
		if (result.isEmpty()) {
			stock = (Stock) ds.insertRecord(stock);
		} else
			stock = result.get(0);

		return stock;

	}

	private Dbxref AddDbXref(DbXrefService dbXrefDs, String textContent) {
		Dbxref dbXref = new Dbxref();
		dbXref.setAccession(textContent);
		dbXref.setDb(db);
		dbXref.setVersion("1");

		List<Dbxref> result = dbXrefDs.findByDbAndAccession(textContent, db);
		if (result.isEmpty()) {
			dbXref = (Dbxref) dbXrefDs.insertRecord(dbXref);
		} else
			dbXref = result.get(0);

		return dbXref;

	}

	private StockSample AddStockSample(StockSampleService stockSampleDs, Stock stock, Dbxref dbXref,
			Integer hdf5Index) {
		StockSample ss = new StockSample();
		ss.setDbxref(dbXref);
		ss.setStock(stock);
		ss.setHdf5Index(hdf5Index);

		return (StockSample) stockSampleDs.insertRecord(ss);

	}

	private SampleVarietyset AddSampleVarietySet(SampleVarietySetService ds, StockSample ss, Db db, Integer hdf5Index) {
		SampleVarietyset svs = new SampleVarietyset();
		svs.setDbId(db.getDbId());
		svs.setStockSampleId(ss.getStockSampleId());
		svs.setHdf5Index(hdf5Index);

		return (SampleVarietyset) ds.insertRecord(svs);

	}

}
