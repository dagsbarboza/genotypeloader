package org.irri.genotype.script.loaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.irri.genotype.ComponentConstants;
import org.irri.genotype.LoaderConstants;
import org.irri.genotype.LoaderProperties;
import org.irri.genotype.script.loaders.utils.MyFileReader;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Feature;
import chado.loader.model.Featureloc;
import chado.loader.model.Organism;
import chado.loader.service.CvService;
import chado.loader.service.CvTermService;
import chado.loader.service.DbService;
import chado.loader.service.FeatureLocService;
import chado.loader.service.FeatureService;
import chado.loader.service.OrganismService;
import chado.loader.service.SnpFeatureLocService;
import chado.loader.service.VariantSetService;

public class GeneLoader2 {

	private static HashMap<String, String> properties;
	private static LoaderProperties prop;
	private static DbService db_ds;
	private static OrganismService org_ds;
	private static CvTermService cvTerm_ds;
	private static VariantSetService v_ds;
	private static CvService cv_ds;
	private static Connection conn;

	private static Logger logger = Logger.getLogger(GeneLoader2.class.getName());

	public static void main(String[] args) {

		initConnectionMap();

		OrganismService os = new OrganismService();
		CvTermService cvt_s = new CvTermService();
		FeatureService f_s = new FeatureService();
		FeatureLocService fl_s = new FeatureLocService();

		Map<String, Feature> chrMap = new HashMap<>();

		chrMap.put("Chr01", f_s.getFeatureById(7651232));
		chrMap.put("Chr02", f_s.getFeatureById(7651233));
		chrMap.put("Chr03", f_s.getFeatureById(7651234));
		chrMap.put("Chr04", f_s.getFeatureById(7651235));
		chrMap.put("Chr05", f_s.getFeatureById(7651236));
		chrMap.put("Chr06", f_s.getFeatureById(7651237));
		chrMap.put("Chr07", f_s.getFeatureById(7651238));
		chrMap.put("Chr08", f_s.getFeatureById(7651239));
		chrMap.put("Chr09", f_s.getFeatureById(7651240));
		chrMap.put("Chr10", f_s.getFeatureById(7651241));
		chrMap.put("Chr11", f_s.getFeatureById(7651242));
		chrMap.put("Chr12", f_s.getFeatureById(7651243));

		int id = 7671902;

		BufferedReader sampleFileReader = MyFileReader.getBufferedReader("/Users/lhbarboza/Downloads/MH63_Content.txt");
		String line;

		if (sampleFileReader != null) {

			List<Organism> organismList = os.findOrganismId(23);
			List<Cvterm> cvtermList = cvt_s.findCvtermyId(502);

			Map<String, Cvterm> cvMap = new HashMap<>();

//			List max_result = AppContext.getEntityManager().createNativeQuery("select max(feature_id) from feature").getResultList();

			try {

				boolean resume = false;
//				Integer id = Integer.parseInt(max_result.get(0).toString()) + 1;
				while ((line = sampleFileReader.readLine()) != null) {

					String[] token = line.split("\t");

					if (token[1].equals("gene")) {

						if (token[4].equals("OsMH63_08G0093100")) {

							resume = true;
						}

						if (resume) {
							if (cvMap.get(token[1]) == null) {
								cvtermList = cvt_s.findCvtermyName(token[1]);
								if (cvtermList.size() == 0)
									System.out.println("did not find" + token[1]);
								else {
									cvMap.put(token[1], cvtermList.get(0));
								}
							}

							String name = token[4];
							Integer max = Integer.parseInt(token[3].trim());
							Integer min = Integer.parseInt(token[2].trim());

							Feature feature = null;
							Featureloc featureloc = null;

							feature = new Feature();
							featureloc = new Featureloc();
							// feature.setFeatureId(id);
							feature.setOrganism(organismList.get(0));
							feature.setCvterm(cvMap.get(token[1]));

							feature.setName(name);
							feature.setUniquename(name);
							feature.setSeqlen(max - min);

//					id++;
//						System.out.println(token[0] +" - " +name +" - "+ feature.getSeqlen());

							feature = f_s.insertRecord(feature);

							featureloc.setFeaturelocId(id);
							featureloc.setFeature1(feature);
							featureloc.setFeature2(chrMap.get(token[0]));
							featureloc.setFmin(min);
							featureloc.setFmax(max);
							featureloc.setStrand(1);
							featureloc.setIsFmaxPartial(false);
							featureloc.setIsFminPartial(false);
							featureloc.setLocgroup(0);
							featureloc.setRank(0);
							System.out.println("feature:" + feature.getFeatureId() + " - FeatureSrc"
									+ chrMap.get(token[0]).getName());

							fl_s.insertRecord(featureloc);
							id++;
						}

//				Stock stock = AddStock(ds, textContent);
//				Dbxref dbXref = AddDbXref(dbXrefDs, textContent);
//
//				StockSample stockSample = AddStockSample(stockSampleDs, stock, dbXref, hdfCounter);
//				SampleVarietyset svs = AddSampleVarietySet(sampleVaritySetDs, stockSample, db, hdfCounter);

//				hdfCounter++;

//				counter++;
					}
					line = null;

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		db_ds = new DbService();
		org_ds = new OrganismService();
		cvTerm_ds = new CvTermService();
		v_ds = new VariantSetService();
		cv_ds = new CvService();

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://" + prop.getHostname() + "/" + prop.getDatabasename(),
					prop.getUsername(), prop.getPassword());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void initConnectionMap() {
		properties = new HashMap<String, String>();
		properties.put(LoaderConstants.HOSTNAME, ComponentConstants.HOSTNAME);
		properties.put(LoaderConstants.PORT, ComponentConstants.PORT);
		properties.put(LoaderConstants.DATABASE, ComponentConstants.DATABASE);
		properties.put(LoaderConstants.USER, ComponentConstants.USERNAME);
		properties.put(LoaderConstants.PASSWORD, ComponentConstants.PASSWORD);

		properties.put(LoaderConstants.CONFIG_NAME, "DEFAULT");
		prop = new LoaderProperties();
		prop.setHostname(ComponentConstants.HOSTNAME);
		prop.setPort(ComponentConstants.PORT);
		prop.setDatabasename(ComponentConstants.DATABASE);
		prop.setUsername(ComponentConstants.USERNAME);
		prop.setPassword(ComponentConstants.PASSWORD);

		AppContext.createEntityManager(properties);

	}

}
