package org.irri.genotype.script.loaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.irri.genotype.ComponentConstants;
import org.irri.genotype.LoaderConstants;
import org.irri.genotype.LoaderProperties;
import org.irri.genotype.script.loaders.utils.MyFileReader;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Feature;
import chado.loader.model.Organism;
import chado.loader.service.CvService;
import chado.loader.service.CvTermService;
import chado.loader.service.DbService;
import chado.loader.service.FeatureService;
import chado.loader.service.OrganismService;
import chado.loader.service.VariantSetService;

public class ChromosomeLoader {

	private static HashMap<String, String> properties;
	private static LoaderProperties prop;
	private static DbService db_ds;
	private static OrganismService org_ds;
	private static CvTermService cvTerm_ds;
	private static VariantSetService v_ds;
	private static CvService cv_ds;
	private static Connection conn;

	private static Logger logger = Logger.getLogger(ChromosomeLoader.class.getName());

	public static void main(String[] args) {

		initConnectionMap();
		
		OrganismService os = new OrganismService();
		CvTermService cvt_s = new CvTermService();
		FeatureService f_s = new FeatureService();
		
				
		
		BufferedReader sampleFileReader = MyFileReader.getBufferedReader("/Users/lhbarboza/Downloads/CHR.txt");
		String line;

		if (sampleFileReader != null) {
			
			List<Organism> organismList = os.findOrganismId(23);
			List<Cvterm> cvtermList = cvt_s.findCvtermyId(502);
			
			logger.info("Loading Sample File.... \\n"+ organismList.size());
			
//			List max_result = AppContext.getEntityManager().createNativeQuery("select max(feature_id) from feature").getResultList();

			try {
				Feature feature = null;
//				Integer id = Integer.parseInt(max_result.get(0).toString()) + 1;
				while ((line = sampleFileReader.readLine()) != null) {

					
					String[] token = line.split(" ");
					String chr = token[0];
					String length = token[1];
					String name = "";
					String uniqueName = "";
					String chromosome = "";
							
					
					String chrNumber = chr.substring(3, chr.length());
					
//					System.out.println("Organism: "+ organismList.get(0).getCommonName());
					//System.out.println("Cvterm 502: "+ cvtermList.get(0).getName());
//					System.out.println("Chr: "+ chr + ": length="+ length +": "+ chrNumber);
					
					if (chrNumber.length() == 2 && chrNumber.startsWith("0")) {
						name = "Chr"+chrNumber.substring(1,chrNumber.length());
						uniqueName = chrNumber;
					}else if (chrNumber.length() == 1) {
						name = "Chr"+chrNumber;
						uniqueName = "chr0"+chrNumber;
					}else {
						name = "Chr"+chrNumber;
						uniqueName = "chr"+chrNumber;
					}
						
					
					
//					System.out.println("FeatureID "+id+" Final Name: "+ name + ": uniqueName="+ uniqueName);	
						
					
							
					
					feature = new Feature();
					//feature.setFeatureId(id);
					feature.setOrganism(organismList.get(0));
					feature.setCvterm(cvtermList.get(0));
					feature.setName(name);
					feature.setUniquename(uniqueName);
					feature.setSeqlen(Integer.parseInt(token[1].trim()));
					
//					id++;
					
					f_s.insertRecord(feature);

//				Stock stock = AddStock(ds, textContent);
//				Dbxref dbXref = AddDbXref(dbXrefDs, textContent);
//
//				StockSample stockSample = AddStockSample(stockSampleDs, stock, dbXref, hdfCounter);
//				SampleVarietyset svs = AddSampleVarietySet(sampleVaritySetDs, stockSample, db, hdfCounter);

					line = null;
//				hdfCounter++;

//				counter++;

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		

		db_ds = new DbService();
		org_ds = new OrganismService();
		cvTerm_ds = new CvTermService();
		v_ds = new VariantSetService();
		cv_ds = new CvService();

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://" + prop.getHostname() + "/" + prop.getDatabasename(),
					prop.getUsername(), prop.getPassword());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void initConnectionMap() {
		properties = new HashMap<String, String>();
		properties.put(LoaderConstants.HOSTNAME, ComponentConstants.HOSTNAME);
		properties.put(LoaderConstants.PORT, ComponentConstants.PORT);
		properties.put(LoaderConstants.DATABASE, ComponentConstants.DATABASE);
		properties.put(LoaderConstants.USER, ComponentConstants.USERNAME);
		properties.put(LoaderConstants.PASSWORD, ComponentConstants.PASSWORD);

		properties.put(LoaderConstants.CONFIG_NAME, "DEFAULT");
		prop = new LoaderProperties();
		prop.setHostname(ComponentConstants.HOSTNAME);
		prop.setPort(ComponentConstants.PORT);
		prop.setDatabasename(ComponentConstants.DATABASE);
		prop.setUsername(ComponentConstants.USERNAME);
		prop.setPassword(ComponentConstants.PASSWORD);
		
		AppContext.createEntityManager(properties);

	}

}
