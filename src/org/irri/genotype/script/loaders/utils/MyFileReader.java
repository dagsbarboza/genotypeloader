package org.irri.genotype.script.loaders.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class MyFileReader {

	public static BufferedReader getBufferedReader(String file) {
		if (file != null)
			try {
				return new BufferedReader(new FileReader(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		return null;
	}

}
