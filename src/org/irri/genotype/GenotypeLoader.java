package org.irri.genotype;

import java.util.HashMap;
import java.util.List;

import org.irri.genotype.components.AddFeatureTypeDialog;
import org.irri.genotype.components.LoadingDialog;

import chado.loader.AppContext;
import chado.loader.model.Cvterm;
import chado.loader.model.Db;
import chado.loader.model.Organism;
import chado.loader.model.Variantset;
import chado.loader.service.CvService;
import chado.loader.service.CvTermService;
import chado.loader.service.DbService;
import chado.loader.service.OrganismService;
import chado.loader.service.VariantSetService;

public class GenotypeLoader {

	private static HashMap<String, String> properties;
	private static LoaderProperties prop;
	private static DbService db_ds;
	private static OrganismService org_ds;
	private static CvTermService cvTerm_ds;
	private static VariantSetService v_ds;
	private static CvService cv_ds;

	public static void main(String[] args) {
		String cvtermName = args[0];
		String organismName = args[1];
		String dbName = args[2];
		String variantset = args[3];
		
		String sampleFile = args[4];
		String posFile = args[5];
		String h5 = args[6];
		
		for (int i = 0; i < 7 ; i++) {
			System.out.println("Loading args["+i+"] : " +args[i]);
		}
		
		
		AppContext.createEntityManager();

		initConnectionMap();
		
		db_ds = new DbService();
		org_ds = new OrganismService();
		cvTerm_ds = new CvTermService();
		v_ds = new VariantSetService();
		cv_ds = new CvService();
		
		Cvterm cvterm = cvTerm_ds.findCvtermyName(cvtermName).get(0);
		Organism organism = org_ds.findOrganismByCommonName(organismName).get(0);
		Db db = db_ds.getDbByName(dbName).get(0);
		Variantset vSet = v_ds.getVariantSetByName(variantset).get(0);
		List<Cvterm> featureCvTermList = cvTerm_ds.getFeatureType(ComponentConstants.CHROMOSOME,
				ComponentConstants.SEQUENCE);

		Cvterm nonSynTerm = cvTerm_ds.findCvtermyName(ComponentConstants.NON_SYNONYMOUS).get(0);
		Cvterm spliceAccTerm = cvTerm_ds.findCvtermyName(ComponentConstants.SPLICE_ACC).get(0);
		Cvterm spliceDnrTerm = cvTerm_ds.findCvtermyName(ComponentConstants.SPLICE_DNR).get(0);

		if (!featureCvTermList.isEmpty()) {
			Cvterm featureCvTerm = featureCvTermList.get(0);
//			LoadingDialog loading = new LoadingDialog(display, shlGenotypeLoader, fcl, fcl2, fcl3, fcl4,
//					fcl5, fcl6, cvterm, organism, db, vSet, featureCvTerm, nonSynTerm, spliceAccTerm,
//					spliceDnrTerm, msa.getPropertyFile());
			
			LoadDataset loading = new LoadDataset(sampleFile, posFile, h5,  cvterm, organism, db, vSet, featureCvTerm, nonSynTerm, spliceAccTerm,
					spliceDnrTerm, prop);
			loading.open();
		} else {
			System.out.println("No implmentation");
		}

	}

	private static void initConnectionMap() {
		properties = new HashMap<String, String>();
		properties.put(LoaderConstants.HOSTNAME, ComponentConstants.HOSTNAME);
		properties.put(LoaderConstants.PORT, ComponentConstants.PORT);
		properties.put(LoaderConstants.DATABASE, ComponentConstants.DATABASE);
		properties.put(LoaderConstants.USER, ComponentConstants.USERNAME);
		properties.put(LoaderConstants.PASSWORD, ComponentConstants.PASSWORD);

		properties.put(LoaderConstants.CONFIG_NAME, "DEFAULT");
		prop = new LoaderProperties();
		prop.setHostname(ComponentConstants.HOSTNAME);
		prop.setPort(ComponentConstants.PORT);
		prop.setDatabasename(ComponentConstants.DATABASE);
		prop.setUsername(ComponentConstants.USERNAME);
		prop.setPassword(ComponentConstants.PASSWORD);

	}

}
