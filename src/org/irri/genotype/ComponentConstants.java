package org.irri.genotype;

public class ComponentConstants {

	public static final String HOSTNAME = "192.168.15.34";

	public static final String PORT = "5432";

	public static final String DATABASE = "iric";

	public static final String USERNAME = "iricadmin";

	public static final String PASSWORD = "iricadmin";

	public static final String NON_SYNONYMOUS = "nonsynonymous_variant";

	public static final String SPLICE_ACC = "splice_acceptor_variant";

	public static final String SPLICE_DNR = "splice_donor_variant";

	public static final String COMBO_SEPARATOR = "-------";

	public static final String CHROMOSOME = "chromosome";

	public static final String SEQUENCE = "sequence";
	
	public static final String ADD_NEW = "Add New..";
	
	public static final String HEADER = "Rice SNP-Seek Database Loader";
}
